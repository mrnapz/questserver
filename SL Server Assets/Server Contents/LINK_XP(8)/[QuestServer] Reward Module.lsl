string reward;
string password;
string nexusAPI;
integer configured = FALSE;
integer LINK_DISPENSE = 4;
integer LINK_BUFFER = 3;
integer LINK_SVR = 5;
integer LINK_DB = 6;
integer LINK_XP = 8;
integer rewardMode;
integer listenchannel; 
integer clicked = 0;
integer DebitPerms;
integer cap;
integer pot;
integer debug;
string secret;
string tableName;
string instance;
string xpType;
key TransactionID = NULL_KEY;
key reqid = NULL_KEY;
string STATE;
string curType;
string operand;
//Crypt
string encrypt(string data){
        // Use a HARD password ! with caps nocaps numbers and symbols !
        //string pass = "Xm2RA6xCQjkpyxYE";
        // Enccrypting the data:
        //string crypt = llXorBase64(llStringToBase64(data), llStringToBase64(pass));
        // Say the mess you made to Owner
        //return crypt;
        return llStringToBase64(llSHA1String(data));
} 
    
award(string addonpassword,string targetusername,integer howmuch,string description) {
    llRegionSay(‭600773468‬,llSHA1String(((string)llGetKey())+addonpassword)+"|addxp|"+targetusername+"|"+(string)howmuch+"|"+description);
}
transact(string slname, integer amount){
    if (amount < 0) {
        amount = amount * -1;
        operand = "minus";
    }else{
        operand = "plus";
    }
    STATE = "processing";
    string crypt = encrypt((string) llGetOwner());
    string endpoint = "https://sldb.project-yazu.co.uk/gphud_currency.php?owner=" + crypt + "&table="+tableName+"&secret="+secret+"&slname=" + slname + "&amount=" + (string)amount + "&curtype=" + xpType + "&instance=" + instance + "&operand=" + operand;
    reqid = llHTTPRequest(endpoint, [HTTP_METHOD, "GET"], "");
}

default {
    state_entry() {
        if (!configured){
            llMessageLinked(LINK_ROOT, 0, "CONFIG_GET", "");
            state config;
        } 
        if (rewardMode == 1){
                state run;
        } else if (rewardMode == 2){
                state linden;
        } else if (rewardMode == 3){
                state rolepay_init;
        } else if (rewardMode == 4){
                state gphud;
        } else if (rewardMode == 5){
                state gphud_currency;
        } else if (rewardMode == 0){
            state disabled;
        }
    }
}

state run {
    state_entry() {
        llSetText("[RPHud] Interface Ready", < 1, 1, 1 > , 1);
    }

    link_message(integer link, integer api_id, string caller, key response) {
        if (caller == "CONFIG"){ state config; }
        if (link == LINK_SVR) {
            if (debug == 2) llSay(0, "got '" + caller + "' From Server, Attempting Transaction");
            llSetText("[RPHud] Processing...", < 1, 1, 1 > , 1);
            llSay(0,"A point of xp was given to "+llKey2Name(caller));
            award(password,llKey2Name(caller),(integer)reward,"Quest Server");
            llSetText("[RPHud] Interface Ready", < 1, 1, 1 > , 1);
            llMessageLinked(LINK_SVR, 0, "OK", NULL_KEY);

        }
    }
}

state linden {
    state_entry() {
        llRequestPermissions(llGetOwner(), PERMISSION_DEBIT);
        llSetText("[L$ Reward] Requesting Debit Permissions", < 1, 1, 0 > , 1);
        llSetPayPrice(0, [0 ,PAY_HIDE, PAY_HIDE, PAY_HIDE]);
    }  
    run_time_permissions (integer perm)
    {
        if  (perm & PERMISSION_DEBIT){
            DebitPerms = TRUE;
            llSetText("[L$ Reward]  Interface Ready \n Current Pot: [L$"+(string)pot+"]", < 1, 1, 1 > , 1);
        } else {
            llSetText("[L$ Reward]  DEBIT ACCESS DENIED!!! \n Please Reset this module", < 1, 0, 0 > , 1);
        }
            
    } 
     money(key id, integer amount)        
    {
        pot+=amount;
        llInstantMessage(id, "Your contribution has been added to the pot, thank you!");
        llSetText("[L$ Reward]  Interface Ready \n Current Pot: [L$"+(string)pot+"]", < 1, 1, 1 > , 1);
        
    }
    
    link_message(integer link, integer api_id, string caller, key response) {
        if (caller == "CONFIG"){ state config; }
        if (link == LINK_SVR) {
            if (debug == 2) llSay(0, "got '" + caller + "' From Server, Attempting Transaction");
            llSetText("[L$ Reward]  Processing...", < 1, 1, 1 > , 1);
            llSay(0,"attempting transaction to "+llKey2Name(caller));
            if (pot >= (integer)reward){
                TransactionID = llTransferLindenDollars(caller, (integer)reward);
            } else {
                llOwnerSay("One of your servers has nothing left in the pot, "+llKey2Name(caller)+" has not recieved their L$"+(string)reward+" reward however this failure was noted and they can retry later.");
                llSleep(1);
                llSetText("[L$ Reward]  TRANSACTION ERROR!!! \n Insufficient Funds in pot! \n Pot: [L$"+(string)pot+"]\n Reward: [L$"+(string)reward+"]", < 1, 0, 0 > , 1);
                llMessageLinked(LINK_SVR, 0, "ERR", NULL_KEY);
            }
            
        }
    }
    transaction_result(key id, integer success, string data)
    {
        if (id != TransactionID)
            return;          // Ignore if not the expected transaction
 
        if (success)
        {
            pot = pot - (integer)reward;
            llOwnerSay( "Transfer of L$ to " + llKey2Name(id) + " was successful");
            llMessageLinked(LINK_SVR, 0, "OK", NULL_KEY);
            llSetText("[L$ Reward]  Interface Ready \n Current Pot: [L$"+(string)pot+"]", < 1, 1, 1 > , 1);
            llSleep(1.0);
        }
        else
        {
            llOwnerSay( "Transfer of L$ failed");
            TransactionID = NULL_KEY;
             llMessageLinked(LINK_SVR, 0, "ERR", NULL_KEY);
             llSetText("[L$ Reward]  TRANSACTION ERROR!!! \n Please Reset this module", < 1, 0, 0 > , 1);
            // Keep the script running so Fred can try again, clear TransactionID to allow a new attempt
        }
    }
    
}

state rolepay_init {
    state_entry()
    {
       llSetText("[NEXUS] Init...", < .1, .3, 1 > , 1);
       reqid = llHTTPRequest("https://rolepay.nexus-sl.net/api/action/check_key/api_key/"+nexusAPI,[HTTP_METHOD,"GET"],"");         
    }
    link_message(integer link, integer api_id, string caller, key response) {
        if (caller == "CONFIG"){ state config; }
    }
    http_response(key i, integer s, list m, string b) {
        if(i == reqid) {
            if(s == 200) {
                b = llGetSubString(b,1,llStringLength(b));
                if(llJsonGetValue(b,["success"]) == "api_key found") {
                    llSetText("[NEXUS]\nAPI Key Valid!\n:",<0.5,1,0.5>,1);
                    llSleep(1);
                    state rolepay;
                } else {
                    llOwnerSay("\n[NEXUS] ERROR : "+llJsonGetValue(b,["error"])+".
                    
If you do not know where to find your API Key, navigate to the Nexus RolePay dashboard using the RolePay server and look under 'API Keys'. If you are not the network owner or do not have access to the dashboard, ask the network owner / manager to create an API Key for you and then insert this value in to the server config where the line reads:- 

nexus_api = <YOUR KEY HERE>");
                    state disabled;
                }
            } else
                llSetText("[NEXUS]\nError Contacting Server...\n:",<1,0.5,0.5>,1);
        }
        }
}
state rolepay {
    state_entry() {
        STATE = "ready";
        llSetText("[NEXUS]\nRolePay Active", < .1, .3, 1 > , 1);    
    }
    http_response(key i, integer s, list m, string b) {
     if (STATE == "processing"){
        if(i == reqid) {
            if(s == 200) {
                b = llGetSubString(b,1,llStringLength(b));
                if(llJsonGetValue(b,["success"])) {
                    if (debug == 2) llSay(0,"Transaction Complete");
                    llSetText("[NEXUS]\nComplete!", < .1, .3, 1 > , 1);
                    llMessageLinked(LINK_SVR, 0, "OK", NULL_KEY);
                    llSleep(1);
                    llSetText("[NEXUS]\nRolePay Active", < .1, .3, 1 > , 1);    
                } else {
                    if (debug == 2) llSay(0,"Transaction Error: "+llJsonGetValue(b,["error"]));
                    llMessageLinked(LINK_SVR, 0, "ERR", NULL_KEY);
                }
            } else
                llSetText("[NEXUS]\nError Contacting Server...\n:",<1,0.5,0.5>,1);
        }
     }
    }
    link_message(integer sender, integer flag, string message, key Key) // This script is in the object too.
    {
        if (message == "CONFIG"){ state config; }
        STATE = "processing";
                llSetText("[NEXUS]\nProcessing...", < .1, .3, 1 > , 1);
        // We are expecting this value to only contain the amount, however in special cases, it may also have a second uuid tacked on.
        list args = llCSV2List(message);
        // args[0] should become the amount
        // args[1] should be a second uuid
        string primaryKey = (string) Key;
        string amount = llList2String(args,0);
        string secondaryKey = llList2String(args,1);
        reqid = llHTTPRequest("https://rolepay.nexus-sl.net/api/action/give_money/api_key/"+nexusAPI+"/to/"+message+"/am/"+reward,[HTTP_METHOD,"GET"],"");
    }
    
    

}

state gphud{
     state_entry() {
        llSetText("[GPHUD] Interface Ready\n "+instance, < 1, 1, 1 > , 1);
        STATE = "Ready";
    }   
    link_message(integer link, integer api_id, string caller, key response) {
        if (caller == "CONFIG"){ state config; }
        if (debug == 2){llSay(0, "got '" + caller + "' From Server, Attempting Transaction");}    
        if (link == LINK_SVR) {
        STATE = "processing";
        string slname = llKey2Name(caller); //convert key to name
        string crypt = encrypt((string)llGetOwner());
        string endpoint = "https://sldb.project-yazu.co.uk/gphud.php?owner="+crypt+"&table="+tableName+"&secret="+secret+"&slname="+slname+"&reward="+reward+"&xptype="+xpType+"&instance="+instance;        
        reqid = llHTTPRequest(endpoint,[HTTP_METHOD,"GET"],"");  
        

        }
        
    }
     http_response(key i, integer s, list m, string b) {
     if (STATE == "processing"){
        if (debug == 2) {llSay(0,b);}
        if(i == reqid) {
            if(s == 200) {                
                if((string)llJsonGetValue(b,["responsetype"]) == (string)"OKResponse") {
                    if (debug == 2){llSay(0,"Transaction Complete");}
                    llSetText("[GPHUD]\nComplete!", < .1, .3, 1 > , 1);
                    llMessageLinked(LINK_SVR, 0, "OK", NULL_KEY);
                    llSleep(1);
                    llSetText("[GPHUD] Interface Ready\n "+instance, < 1, 1, 1 > , 1);   
                } else {
                    if (debug == 2){ llSay(0,"Transaction Error: "+llJsonGetValue(b,["responsetype"]));}
                    llMessageLinked(LINK_SVR, 0, "ERR", NULL_KEY); // Inform the logic unit there was an error.
                }
            } else
                llSetText("[GPHUD]\nError Contacting Server...\n:",<1,0.5,0.5>,1);
        }
     }
    } 
    
}
state gphud_currency{
     state_entry() {
        llSetText("[GPHUD $] Interface Ready\n "+instance, < 1, 1, 1 > , 1);
        STATE = "Ready";
    }   
    link_message(integer link, integer api_id, string caller, key response) {
        if (caller == "CONFIG"){ state config; }
        if (debug == 2){llSay(0, "got '" + caller + "' From Server, Attempting Transaction");}    
        if (link == LINK_SVR) {
            STATE = "processing";
            transact(llKey2Name( caller ), (integer)reward);
        }
        
    }
     http_response(key i, integer s, list m, string b) {
     if (STATE == "processing"){
        if (debug == 2) {llSay(0,b);}
        if(i == reqid) {
            if(s == 200) {                
                if((string)llJsonGetValue(b,["responsetype"]) == (string)"OKResponse") {
                    if (debug == 2){llSay(0,"Transaction Complete");}
                    llSetText("[GPHUD $]\nComplete!", < .1, .3, 1 > , 1);
                    llMessageLinked(LINK_SVR, 0, "OK", NULL_KEY);
                    llSleep(1);
                    llSetText("[GPHUD $] Interface Ready\n "+instance, < 1, 1, 1 > , 1);   
                } else {
                    if (debug == 2){ llSay(0,"Transaction Error: "+llJsonGetValue(b,["responsetype"]));}
                    llMessageLinked(LINK_SVR, 0, "ERR", NULL_KEY); // Inform the logic unit there was an error.
                }
            } else
                llSetText("[GPHUD]\nError Contacting Server...\n:",<1,0.5,0.5>,1);
        }
     }
    } 
    
}

state config {
    state_entry(){
        llSetText("!CONFIG", <1,0,0>,1.0);   
        llSetPayPrice(0, [PAY_HIDE ,PAY_HIDE, PAY_HIDE, PAY_HIDE]);
    }
    link_message(integer sender_num, integer num, string msg, key id) {
        list data = llCSV2List(msg);
        string var = llList2String(data, 0);
        string val = llList2String(data, 1);
        if (var == "rewardMode"){
            rewardMode = (integer)val;
        }
        if (var == "reward"){
            reward = val;
        }
        if (var == "hudpassword"){
            password = val;
        }
        if (var == "nexus_api"){
            nexusAPI = val;
        }
        if (var == "cap"){
            cap = (integer)val;
            pot += cap;
        }
        if (var == "INIT"){
            configured = TRUE;
            state default;
        }
        //Used for GPHud bridge security
        if (var == "key"){
            secret = val;
        }
        if (var == "db"){
            tableName = val;
        }
        if (var == "instance"){
            instance = val;
        }
        if (var == "xptype"){
            xpType = val;
        }
        if (var == "debug") {
            debug = (integer) val;
        }
        
    }
}
state disabled {
    state_entry() {
        llSetText("[DISABLED]", < .5, .5, .5 > , 1);
    }

    link_message(integer link, integer api_id, string caller, key response) {
        if (caller == "CONFIG"){ state config; }
        if (link == LINK_SVR) {
            llMessageLinked(LINK_SVR, 0, "OK", NULL_KEY);
        }
    }

}