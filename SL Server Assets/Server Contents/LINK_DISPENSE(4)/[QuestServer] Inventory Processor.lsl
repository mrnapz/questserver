integer LINK_DISPENSE = 4;
integer LINK_BUFFER = 3;
integer LINK_SVR = 5;
integer LINK_DB = 6;
integer LINK_XP = 8;
integer configured = FALSE;
integer debug;
// Operator Values to be set as per quest requirements

string starterInventory;
string finisherInventory;

string npcOpenPhrase = "";
string npcClosePhrase = "";
integer npcChatChannel = FALSE; //FALSE to disable

// END Variables
default
{
    state_entry()
    {
        if (!configured){
            llMessageLinked(LINK_ROOT, 0, "CONFIG_GET", "");
            state config;
        } else { 

            if (debug >= 1){
                llSetText("Dispenser IDLE",<1,1,1>,1);
            } else {
                llSetText("", <0,0,0>,0);
            }
        }
    }
    link_message(integer source, integer num, string msg, key id)
    {
        if (msg == "CONFIG"){ state config; }
        list input = llCSV2List(msg);
        key uid = llList2String(input, 0);
        integer item = (integer) llList2String(input, 1); 
        
        if (item == 1){
            if (starterInventory != ""){
                if (debug >= 1){
                llSetText("Dispenser BUSY\n"+(string)uid,<1,1,1>,1);
            } else {
                llSetText("", <0,0,0>,0);
            }
                //give starter
                llGiveInventory(uid, starterInventory);
                if (npcChatChannel){
                    llRegionSay(npcChatChannel, npcOpenPhrase);
                }
            }
        }
        if (item == 2){
            if (finisherInventory != ""){
                if (debug >= 1){
                llSetText("Dispenser BUSY\n"+(string)uid,<1,1,1>,1);
                } else {
                llSetText("", <0,0,0>,0);
                }
                //give starter
                llGiveInventory(uid, finisherInventory);
                if (npcChatChannel){
                    llRegionSay(npcChatChannel, npcClosePhrase);
                }
            }
        }
            if (debug >= 1){
                llSetText("Dispenser IDLE",<1,1,1>,1); 
            } else {
                llSetText("", <0,0,0>,0);
            }
    }
}
state config {
    state_entry(){
        llSetText("!CONFIG", <1,0,0>,1.0);   
    }
    link_message(integer sender_num, integer num, string msg, key id) {
        list data = llCSV2List(msg);
        string var = llList2String(data, 0);
        string val = llList2String(data, 1);
        if (var == "start_item"){
            starterInventory = val;
        }
        if (var == "end_item"){
            finisherInventory = val;
        }
        if (var == "debug"){
            debug = (integer)val;    
        }

        if (var == "INIT"){
            configured = TRUE;
            state default;
        }
        
    }
}
