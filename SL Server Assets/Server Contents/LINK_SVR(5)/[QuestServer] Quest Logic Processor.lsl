integer LINK_DISPENSE = 4;
integer LINK_BUFFER = 3;
integer LINK_SVR = 5;
integer LINK_DB = 6;
integer LINK_XP = 8;
integer HUSH;
integer configured = FALSE;
integer i = 0;
integer r = 0; //Round trip, because we don't need to say complete 5 times yes?
list input;
string init;
key uid;
integer item;
key sender; //API Callback Key/object
string flag;
string obj1;
string obj2;
string obj3;
string obj4;
string obj5;
string complete;
string rewarded;
string svr_state = "IDLE";
string trigger;
integer debug;
integer strictOrder;
integer reportProgress;
//Slave mode Vars
integer slaveMode;
integer masterServerChannel;
integer slaveEnroll;
string slaveObjectiveNumber;

//Encryption 
string ProtocolSignature = "QSVR"; // your own signature
float ProtocolVersion = 0.3; // can range from 0.0 to 255.255
string Password; // change this to your own password
integer communicationsChannel = PUBLIC_CHANNEL;
string Header;
string strHex = "0123456789ABCDEF";

messageServer(integer channel, string message) {
    llRegionSay(channel, encrypt(Password, message));
}
softReset() {
    trigger = "";
    svr_state = "IDLE";
    if (debug == 2) llSay(DEBUG_CHANNEL, "Sending READY signal to buffer");
    if (svr_state == "IDLE") {
        llMessageLinked(LINK_BUFFER, 0, "READY", NULL_KEY);
    }
}

userInDB(string input) {
    llMessageLinked(LINK_DB, 0, "CHK," + input, NULL_KEY);
}
dbWrite(string input) {
    llMessageLinked(LINK_DB, 0, "PUT," + input, NULL_KEY);
}
dbRead(string input) {
    llMessageLinked(LINK_DB, 0, "GET," + input, NULL_KEY);
    if (debug == 2) llSay(DEBUG_CHANNEL, "DB Read called");
}
dbCreate(string input) {
    llMessageLinked(LINK_DB, 0, "NEW," + input, NULL_KEY);
}
reward(string input) {
    llMessageLinked(LINK_XP, 0, input, NULL_KEY);
    if (slaveMode == 1) {
        if (slaveEnroll == 1) {
            messageServer(masterServerChannel, "Initiator," + (string) input + "," + slaveObjectiveNumber);
        } else {
            messageServer(masterServerChannel, "Item," + (string) input + "," + slaveObjectiveNumber);
        }
    }
}
dispense(string input) {
    llMessageLinked(LINK_DISPENSE, 0, input, NULL_KEY);
}

writeProgressBit() {
    // So it begins... 
    //Now we need to work out which bit corresponds to which value we just recieved and write them all back. *sigh*
    if (item == 1) {
        obj1 = "1";
        //dbWrite((string) uid + ",1," + obj2 + "," + obj3 + "," + obj4 + "," + obj5 + "," + complete + "," + rewarded);
    } else if (item == 2) {
        obj2 = "1";
       // dbWrite((string) uid + "," + obj1 + ",1," + obj3 + "," + obj4 + "," + obj5 + "," + complete + "," + rewarded);
    } else if (item == 3) {
        obj3 = "1";
       // dbWrite((string) uid + "," + obj1 + "," + obj2 + ",1," + obj4 + "," + obj5 + "," + complete + "," + rewarded);
    } else if (item == 4) {
        obj4 = "1";
        //dbWrite((string) uid + "," + obj1 + "," + obj2 + "," + obj3 + ",1," + obj5 + "," + complete + "," + rewarded);
    } else if (item == 5) {
        obj5 = "1";
        //dbWrite((string) uid + "," + obj1 + "," + obj2 + "," + obj3 + "," + obj4 + ",1," + complete + "," + rewarded);
    }
    dbWrite((string) uid + "," + obj1 + "," + obj2 + "," + obj3 + "," + obj4 + "," + obj5 + "," + complete + "," + rewarded);
    if (reportProgress) {
        if (!HUSH)
            llInstantMessage((key) uid, "You have completed " + (string)((integer) obj1 + (integer) obj2 + (integer) obj3 + (integer) obj4 + (integer) obj5) + "/5 Objectives");
    }
}

string hex(integer value) {
    integer digit = value & 0xF;
    string text = llGetSubString(strHex, digit, digit);
    value = (value >> 4) & 0xfffFFFF;
    integer odd = TRUE;
    while (value) {
        digit = value & 0xF;
        text = llGetSubString(strHex, digit, digit) + text;
        odd = !odd;
        value = value >> 4;
    }
    if (odd)
        text = "0" + text;
    return text;
}
string encrypt(string password, string message) {
    // get a random value
    integer nonce = (integer) llFrand(0x7FFFFFFF);

    // generate digest and prepend it to message
    message = llMD5String(message, nonce) + message;

    // generate one time pad
    string oneTimePad = llMD5String(password, nonce);

    // append pad until length matches or exceeds message
    integer count = (llStringLength(message) - 1) / 32;
    if (count)
        do
            oneTimePad += llMD5String(oneTimePad, nonce);
        while (--count);

    // return the header, once and encrypted message
    return Header + llGetSubString("00000000" + hex(nonce), -8, -1) + llXorBase64StringsCorrect(llStringToBase64(message), llStringToBase64(oneTimePad));
}


//CSV Format 
// DB flag | uid |     Data
// command , key , 0,0,0,0,0,0,0  
// ["uuid","obj1", "obj2", "obj3", "obj4", "obj5", "complete", "rewarded"])
//   key      0       0       0       0       0       0            0 
default {
    state_entry() {
        if (!configured) {
            llMessageLinked(LINK_ROOT, 0, "CONFIG_GET", "");
            //build the header, it never changes.
            list versions = llParseString2List((string) ProtocolVersion, ["."], []);
            string minor = llList2String(versions, 1);
            integer p = 0;
            while (llGetSubString(minor, --p, p) == "0");
            Header = ProtocolSignature + hex(llList2Integer(versions, 0)) + hex((integer) llGetSubString(minor, 0xFF000000, p));
            state config;
        } else {
            llSetTimerEvent(1);
            if (debug == 2) llSay(DEBUG_CHANNEL, "Sending READY signal to buffer");
            if (svr_state == "IDLE") {
                llMessageLinked(LINK_BUFFER, 0, "READY", NULL_KEY);
            }
        }
    }
    timer() {
        if (debug == 0) llSetText("", < 0, 0, 0 > , 0);
        if (debug >= 1) {
            integer free_memory = llGetFreeMemory();
            integer used_memory = llGetUsedMemory();
            llSetText("SERVER RUNNING \n Memory Free:" + (string) free_memory + " Bytes\n Memory Used:" + (string) used_memory + " Bytes \n Current State: " + svr_state, < 1, 1, 1 > , 1);
        }
    }


    link_message(integer sender_num, integer num, string msg, key id) {
        // process full request from db
        // split csv to variables we can work with?
        // now we need to check all the bits
        if (debug == 2) {
            if (sender_num == LINK_BUFFER) {
                llSay(DEBUG_CHANNEL, "Got next tem from Queue");
            } else {
                llSay(DEBUG_CHANNEL, "Link_Message: " + msg);
            }
        }
        if (debug == 2) llSay(DEBUG_CHANNEL, "Trigger is: " + trigger);
        if (debug == 2) llSay(DEBUG_CHANNEL, "State is: " + svr_state);
        list data = llCSV2List(msg);
        flag = llList2String(data, 0);
        obj1 = llList2String(data, 1);
        obj2 = llList2String(data, 2);
        obj3 = llList2String(data, 3);
        obj4 = llList2String(data, 4);
        obj5 = llList2String(data, 5);
        complete = llList2String(data, 6);
        rewarded = llList2String(data, 7);

        if (flag == "CONFIG") {
            state config;
        }
        //Recieve Requests from Buffer

        if (svr_state == "IDLE" && sender_num == LINK_BUFFER) {


            input = llCSV2List(msg);
            init = llList2String(input, 0);
            uid = llList2String(input, 1);
            item = (integer) llList2String(input, 2);
            sender = (key)llList2String(input, 3);
            //Drop invalid messages
            if ((key) uid != NULL_KEY) {
                if (!HUSH)
                    llInstantMessage(uid, "Processing Your Request, Please wait.");
                llRegionSayTo(sender, -102938, "PROCESSING");
                if (item <= (integer) 5) {
                    if (init == "Initiator") {
                        trigger = "INIT";
                        //user in db?
                        userInDB((string) uid + ",0,0,0,0,0,0,0"); //Call DB Read
                        svr_state = "QUERY"; //Set State machine for linkMessage
                    }
                    if (init == "Item") {
                        trigger = "ITEM";
                        userInDB((string) uid + ",0,0,0,0,0,0,0"); //Call DB Read
                        svr_state = "QUERY"; //Set State machine for linkMessage            
                    }
                } else {
                    llSetText("DROP", < 1, 0, 0 > , 1);
                    jump
                    break; //invalid input so GTFO!
                }
            } else {
                if (!HUSH)
                    llInstantMessage(uid, "The Quest Item Value is out of range [" + (string) item + "] - Only a value of 1-5 is allowed, 0 is reserved/disused.");
            }
        } else

            //Recieve response from DB
            if (sender_num == LINK_DB) {
                if (svr_state == "QUERY") {
                    if (msg == "FOUND") {
                        if (debug == 2) llSay(DEBUG_CHANNEL, "User already in DB");
                        svr_state = "WAIT";
                        dbRead(uid);
                    } else if (msg == "NOT_FOUND") {
                        if (trigger == "INIT") {
                            //create
                            if (debug == 2) llSay(DEBUG_CHANNEL, "User not in DB, new entry needed.");
                            dbCreate((string) uid + ",0,0,0,0,0,0,0");
                            svr_state = "CREATE";
                        }
                        if (trigger == "ITEM") {
                            if (!HUSH)
                                llInstantMessage((key) uid, "You have found an objective, but you have not enrolled. Please find the quest Initiator before proceeding.");
                            llRegionSayTo(sender, -102938, "NOT_ENROLLED");
                            r = 4;
                            svr_state = "IDLE";

                        }
                    }
                }
                if (svr_state == "WAIT") {
                    //Quest Initiator 
                    if (trigger == "INIT") {
                        if (msg == "FOUND") {
                            if (debug == 2) llSay(DEBUG_CHANNEL, "Data was 'Found', waiting for data stream.");
                            svr_state = "WAIT";
                            trigger = "INIT";
                            //dbRead(uid);
                            jump
                            break;
                            // Skip this round because LSL throws data out like cold shit down an icy slope.
                        } else {
                            if ((integer) rewarded != 1) {
                                if ((integer) obj1 == 0 | (integer) obj2 == 0 | (integer) obj3 == 0 | (integer) obj4 == 0 | (integer) obj5 == 0) {
                                    //Not all bits set
                                    if (!HUSH)
                                        llInstantMessage((key) uid, "You have not completed all the objectives.");
                                    llRegionSayTo(sender, -102938, "INCOMPLETE");
                                    r = 4;
                                    svr_state = "IDLE";
                                    trigger = "";

                                } else {
                                    // all bits are satisfied 
                                    dbWrite((string) uid + ",1,1,1,1,1,1,0");
                                    svr_state = "REWARD";
                                    trigger = "";
                                }
                            } else {
                                if (!HUSH)
                                    llInstantMessage((key) uid, "You Already Handed in the quest and got rewarded.");
                                llRegionSayTo(sender, -102938, "ALREADY_DONE");
                                r = 4;
                                svr_state = "IDLE";
                            }
                        }
                    }
                    //End Quest Init

                    //Item init
                    if (trigger == "ITEM") {

                        if (debug == 2) llSay(DEBUG_CHANNEL, "Trigger: ITEM");
                        //Validate input
                        if (msg == "FOUND") {
                            if (debug == 2) llSay(DEBUG_CHANNEL, "Data was 'Found' Validating.");
                            trigger = "ITEM";
                            // Skip this round because LSL throws data out like cold shit down an icy slope.
                        } else {
                            if (debug == 2) llSay(DEBUG_CHANNEL, "Tiggered by Item");
                            if ((integer) complete == 1) {
                                if (!HUSH)
                                    llInstantMessage((key) uid, "You already completed the quest.");
                                llRegionSayTo(sender, -102938, "ALREADY_DONE");
                                r = 4;
                                svr_state = "IDLE";

                            } else {
                                // Strict order enforcement. 
                                if (strictOrder) {
                                    if (item == 5 && obj4 != "1") {
                                        //find 5
                                        if (!HUSH)
                                            llInstantMessage((key) uid, "You have found objective 5 without completing objective 4.");
                                        llRegionSayTo(sender, -102938, "WRONG_ORDER");
                                        softReset();
                                    } else if (item == 4 && obj3 != "1") {
                                        //Find 3
                                        if (!HUSH)
                                            llInstantMessage((key) uid, "You have found objective 4 without completing objective 3.");
                                        llRegionSayTo(sender, -102938, "WRONG_ORDER");
                                        softReset();
                                    } else if (item == 3 && obj2 != "1") {
                                        //Find 2
                                        if (!HUSH)
                                            llInstantMessage((key) uid, "You have found objective 3 without completing objective 2.");
                                        llRegionSayTo(sender, -102938, "WRONG_ORDER");
                                        softReset();
                                    } else if (item == 2 && obj1 != "1") {
                                        //Find 1
                                        if (!HUSH)
                                            llInstantMessage((key) uid, "You have found objective 2 without completing objective 1.");
                                        llRegionSayTo(sender, -102938, "WRONG_ORDER");
                                        softReset();
                                    } else {
                                        writeProgressBit();
                                        llRegionSayTo(sender, -102938, "SUCCESS");
                                    }
                                } else {
                                    writeProgressBit();
                                    llRegionSayTo(sender, -102938, "SUCCESS");
                                }
                                
                                trigger = "";
                                svr_state = "IDLE";

                            }

                        } //Validate input end
                    }

                } //End wait section

                if (svr_state == "CREATE") {
                    if (msg == "OK") {
                        if (!HUSH)
                            llInstantMessage((key) uid, "You have started the quest!");
                        llRegionSayTo(sender, -102938, "ENROLLED");
                        r = 4;
                        dispense((string) uid + ",1"); //Give the starter
                        svr_state = "IDLE";
                    }
                }
                if (svr_state == "REWARD") {
                    if (i == 1) {
                        //this is literally another workaround to LSL being a complete plum.
                        //Why this was being fired off twice is beyond me, but it did so without failure so we'll catch the second round instead
                        if (msg == "OK") {
                            if (!HUSH)
                                llInstantMessage((key) uid, "Well Done! Here's your reward.");
                            llRegionSayTo(sender, -102938, "FINISHED");
                            reward((string) uid);
                            if (debug == 2) llSay(DEBUG_CHANNEL, (string) uid);
                            i = 0;
                        }
                    } else {
                        i++;
                    }
                }
                if (svr_state == "FINALIZE") {
                    if (!HUSH)
                        llInstantMessage((key) uid, "Reward Granted");
                    dispense((string) uid + ",2"); //Give the Finisher
                    svr_state = "IDLE";
                }
            } // end db
        //Recieve response from 
        if (sender_num == LINK_XP) {
            if (msg == "OK") {
                svr_state = "FINALIZE";
                dbWrite((string) uid + ",1,1,1,1,1,1,1");

            } else if (msg == "ERR") {
                if (!HUSH)
                    llInstantMessage((key) uid, "Oops! There was a transaction error, try again later.");
                llRegionSayTo(sender, -102938, "REWARD_ERROR");
                dbWrite((string) uid + ",1,1,1,1,1,1,0");
                svr_state = "IDLE";
            }
        }

        // We end up here 4 times but only on the 5th round are we actually done with the request.
        if (r < 4) {
            r++;
        } else {
            if (!HUSH)
                llInstantMessage(uid, "Process Complete.");
            if (debug == 2) llSay(DEBUG_CHANNEL, "Sending READY signal to buffer");
            llMessageLinked(LINK_BUFFER, 0, "READY", NULL_KEY);
            r = 0;
        }
        @break;
    }
}
state config {
    state_entry() {
        llSetText("!CONFIG", < 1, 0, 0 > , 1.0);
    }
    link_message(integer sender_num, integer num, string msg, key id) {
        
        list data = llCSV2List(msg);
        string
        var = llList2String(data, 0);
        string val = llList2String(data, 1);
        if (var == "INIT") {
            configured = TRUE;
            state
            default;
        }
        if (var == "hush"){
            if(val == "1"){
                HUSH = TRUE;
            }else{ 
                HUSH = FALSE;
            }
        jump end;
        }
        if (var == "slaveMode") {
            slaveMode = (integer) val;
            jump end;
        }
        if (var == "masterServerChannel") {
            masterServerChannel = (integer) val;
            jump end;
        }
        if (var == "slaveEnroll") {
            slaveEnroll = (integer) val;
            jump end;
        }
        if (var == "masterCipher") {
            Password = (string) val;
            jump end;
        }
        if (var == "slaveObjectiveNumber") {
            slaveObjectiveNumber = (string) val;
            jump end;
        }
        if (var == "chatName") {
            llSetObjectName((string) val);
            jump end;
        }
        if (var == "debug") {
            debug = (integer) val;
            jump end;
        }
        if (var == "strict_order") {
            if (val == "1")
                strictOrder = TRUE;
            jump end;
        }
        if (var == "report_progress") {
            if (val == "1")
                reportProgress = TRUE;
        }
        
        @end;
    }
}