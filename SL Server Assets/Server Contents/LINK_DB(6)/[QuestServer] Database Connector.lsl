//***********************************************************************
// SLDB: Simple Database Storage for LSL (version 1.0)
//***********************************************************************
// Copyright (C) 2009 aubreTEC Labs
// http://aubretec.com/products/sldb
//
// This program is free software. You can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//**********************************************************************
//**********************************************************************
// ABOUT THIS SCRIPT
// This is a heavily-commented example script showing you how to store, 
// retrieve, and delete data from your SLDB installation. This assumes
// you have already installed SLDB and are ready to begin storing data.
//**********************************************************************
//**********************************************************************
// VARIABLES - These are necessary variables.
//**********************************************************************
// This is the direct URL to your installation (be sure to include the 
// data.php part)

string tableName;
string url;
string secret;
string apiSvr;

// This is the character you want to use to separate lists.  For most 
// purposes, the pipe ("|") will work; you only need to change this if you 
// have a specific reason to (if you think your stored data might have pipes 
// in it, for instance).
integer debug;
string separator = "|";
string db_state;
integer configured = FALSE;
integer LINK_DISPENSE = 4;
integer LINK_BUFFER = 3;
integer LINK_SVR = 5;
integer LINK_DB = 6;
integer LINK_XP = 8;
list result;
string cmd;
string uid;
string obj1;
string obj2;
string obj3;
string obj4;
string obj5;
string complete;
string rewarded;
string assembled;
// These are the keys to which you'll assign your HTTPRequests(); the 
// http_response event will use this key to distinguish what kind of
// request it's returning data for.
key put_id;
key get_id;
key del_id;

//**********************************************************************
// FUNCTIONS - There is one custom function for each action (storing
// data, retrieving it, and deleting it).  These functions are designed
// to make these tasks much easier.  You should not change these unless
// you really know what you're doing.
//**********************************************************************

// This function adds or updates the data in your database.  Send it the
// list of fields you want update, followed by a list of values (one for
// each field, in the same order as the fields.  The verbose variable
// determines how detailed your response will be.  For most purposes, 
// FALSE is just fine.
string str_replace(string src, string from, string to) { //replaces all occurrences of 'from' with 'to' in 'src'.
    integer len = (~-(llStringLength(from)));
    if (~len) {
        string buffer = src;
        integer b_pos = ERR_GENERIC;
        integer to_len = (~-(llStringLength(to)));
        @loop; //instead of a while loop, saves 5 bytes (and runs faster).
        integer to_pos = ~llSubStringIndex(buffer, from);
        if (to_pos) {
            buffer = llGetSubString(src = llInsertString(llDeleteSubString(src, b_pos -= to_pos, b_pos + len), b_pos, to), (-~(b_pos += to_len)), 0x8000);
            jump loop;
        }
    }
    return src;
}
PutData(key id, list fields, list values, integer verbose) {
    string args;
    args += "?key=" + llEscapeURL(id) + "&action=put&separator=" + llEscapeURL(separator);
    args += "&fields=" + llEscapeURL(llDumpList2String(fields, separator));
    args += "&values=" + llEscapeURL(llDumpList2String(values, separator));
    args += "&secret=" + llEscapeURL(secret);
    args += "&table=" + tableName;
    put_id = llHTTPRequest(url + args, [HTTP_METHOD, "GET", HTTP_MIMETYPE, "application/x-www-form-urlencoded"], "");
}

// This function retrieves the data from the database.  Send it the list
// of fields you want to retrieve. If verbose = TRUE, you will get back
// a string that looks like this: field1|value1|field2|value2. If
// verbose = FALSE, you'll get back just the values: value1|value2|value3
// To retrieve ALL of a user's data, use the list ["ALL_DATA"] in the fields
// variable (though you should DEFINITELY use verbose = TRUE for this).

GetData(key id, list fields, integer verbose) {
    string args;
    args += "?key=" + llEscapeURL(id) + "&action=get&separator=" + llEscapeURL(separator);
    args += "&fields=" + llEscapeURL(llDumpList2String(fields, separator)) + "&verbose=" + (string) verbose;
    args += "&secret=" + llEscapeURL(secret);
    args += "&table=" + tableName;
    get_id = llHTTPRequest(url + args, [HTTP_METHOD, "GET", HTTP_MIMETYPE, "application/x-www-form-urlencoded"], "");
}
DelData(key id, list fields, integer verbose) {
    string args;
    args += "?key=" + llEscapeURL(id) + "&action=del&separator=" + llEscapeURL(separator);
    args += "&fields=" + llEscapeURL(llDumpList2String(fields, separator)) + "&verbose=" + (string) verbose;
    args += "&secret=" + llEscapeURL(secret);
    args += "&table=" + tableName;
    del_id = llHTTPRequest(url + args, [HTTP_METHOD, "GET", HTTP_MIMETYPE, "application/x-www-form-urlencoded"], "");
}

msgServer(string response) {
    llMessageLinked(LINK_SVR, 0, response, NULL_KEY);
}
// This function deletes the data from the database.  Send it the list
// of fields you want to delete. The verbose variable determines how 
// detailed your response will be.  For most purposes, FALSE is just fine.
// To delete ALL of a user's data, use the list ["ALL_DATA"] in the fields
// variable.

default {
    state_entry() {
        if (!configured){
            llMessageLinked(LINK_ROOT, 0, "CONFIG_GET", "");
            state config;
        } else { 
            llSetTimerEvent(1);
        }
    }

    timer() {
        if (debug == 0) llSetText("", <0,0,0>,0);
        if (debug >= 1){
        integer free_memory = llGetFreeMemory();
        integer used_memory = llGetUsedMemory();
        llSetText("DATABASE RUNNING: \n Memory Free:" + (string) free_memory + " Bytes\n Memory Used:" + (string) used_memory + " Bytes \nStatus: " + db_state + "\n Command: " + cmd, < 1, 1, 1 > , 1);
        }
    }
    touch_start(integer total_number) {
        // In this example, we're fetching the owner's name and avatar size (stored above) on touch.
        //GetData(NULL_KEY,["values"],TRUE);
    }

    http_response(key id, integer status, list metadata, string body) {
        // In this example, we're simply spitting back the data we've gotten from the server as an llOwnerSay.

        // First, make sure this request is one of the ones used in this script (as opposed to one being called
        // by another script in the same object).

        if ((id != put_id) && (id != get_id) && (id != del_id)) return;

        // If the status isn't 200, then there was a problem connecting to your server.  Maybe the URL isn't
        // correct, or the server is offline.  Set the body to the server error so the final result is an
        // accurate account of what happened.
        if (status != 200) body = "ERROR: CANNOT CONNECT TO SERVER";

        // And spit out the information we got.
        result = llCSV2List(str_replace(body, "|", ","));
        db_state = body;
        if (cmd == "CHK") {
            if (llList2String(result, 1) == "NO_DATA") {
                //user not found
                msgServer("NOT_FOUND");
            } else {

                msgServer("FOUND");
            }

        }
        if (cmd == "GET") {
            msgServer(str_replace(body, "|", ","));
            if (debug == 2) llSay(DEBUG_CHANNEL,"Sending back data: "+str_replace(body,"|", ","));
        }
        if (cmd == "PUT") {
            PutData(uid, ["values"], [assembled], FALSE);
            cmd = "WRITEBACK";
        }
        if (cmd == "NEW") {
            msgServer("OK");
        }
        if (cmd == "WRITEBACK") {
            msgServer("OK");
        }
    }
    link_message(integer sender_num, integer num, string msg, key id) {
        if (msg == "CONFIG"){ state config; }
        if (debug == 2) llSay(DEBUG_CHANNEL, "Got: "+(string)msg);
        if (sender_num == LINK_SVR) {

            // "uuid","obj1", "obj2", "obj3", "obj4", "obj5", "complete", "rewarded"
            // the first value in our csv is our request command CHECK, GET or PUT
            list input = llCSV2List(msg); // Cram it all in to a list
            cmd = llList2Key(input, 0); // now splice it up in to variables 
            uid = llList2Key(input, 1);
            obj1 = llList2Key(input, 2);
            obj2 = llList2Key(input, 3);
            obj3 = llList2Key(input, 4);
            obj4 = llList2Key(input, 5);
            obj5 = llList2Key(input, 6);
            complete = llList2Key(input, 7);
            rewarded = llList2Key(input, 8);
            assembled = obj1 + "," + obj2 + "," + obj3 + "," + obj4 + "," + obj5 + "," + complete + "," + rewarded;
            if (cmd == "CHK") {
                // check if uid is in db, some unique handling is needed here    
                GetData(uid, ["values"], TRUE);
            }
            if (cmd == "GET") {
                //raw get and return
                GetData(uid, ["values"], TRUE);
            }
            if (cmd == "PUT") {
                //update function - becuse the scrpt lacks an update call we have to erase and re-write
                DelData(uid, ["values"], TRUE);
            }
            if (cmd == "NEW") {
                //new person!
                if (debug == 2) llSay(DEBUG_CHANNEL,uid+assembled);
                PutData(uid, ["values"], [assembled], FALSE);
            }

        }
    }
}

state config {
    state_entry(){
        llSetText("!CONFIG", <1,0,0>,1.0);   
    }
    link_message(integer sender_num, integer num, string msg, key id) {
        list data = llCSV2List(msg);
        string var = llList2String(data, 0);
        string val = llList2String(data, 1);
        if (var == "apiSvr"){
            apiSvr == val;
        }
        if (var == "db"){
            tableName = val;
            url = apiSvr+"/api/"+tableName+"/index.php";
        }
        if (var == "key"){
            secret = val;
        }
        if (var == "debug"){
            debug = (integer)val;
        }
        if (var == "INIT"){
            configured = TRUE;
            state default;
        }
        
    }
}
