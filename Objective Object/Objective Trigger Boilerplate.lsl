// This is a simple example showing how to use the [QuestServer] Object API
// This example shows a call to the API using a Linkset Message, simply sending the UUID of the resident who activated it with the touch_start event trigger. 
// this is the basic requirement, however more advanced features are handled and done for you in the API script
// See the !API_CONFIG notecard for more options 

//Object Number constant
// 0     - allow API to decide based on !API_CONFIG setting objectNum
// 1-5   - Specify any objective number from 1 to 5
// 6     - for the quest initiator role. 

integer OBJECT_NUMBER = 0;
 
integer gListener;     // Identity of the listener associated with the dialog, so we can clean up when not needed
default
{
    state_entry()
    {
        //Hint - Use the key you see in local chat to lock the listener to the server
        // Replace NULL_KEY with the "UUID"; of your server;  
        key server = NULL_KEY;
        
        llListen(-102938,"","",""); //Listen for callbacks from the server
    }
    touch_start(integer total_number){
        // get the UUID of the person touching this prim
        key user = llDetectedKey(0);
        // Listen to any reply from that user only, and only on the same channel to be used by llDialog
        // It's best to set up the listener before issuing the dialog
        // Send a dialog to that person. We'll use a fixed negative channel number for simplicity
        llMessageLinked(LINK_THIS, OBJECT_NUMBER, (string)user, NULL_KEY); //Inform Quest Server a player wants to begin the game
        
        // You may also specifiy an object number manually, this can override the one specified in the API scrtipt config
        // This is useful for sending multiple registration events from a single object or trigger/event. 
        
        // llMessageLinked(LINK_THIS, 1, (string)user, NULL_KEY); // Set the 1 to any number as outlined at the top of the script.

    }
    listen(integer chan, string name, key id, string msg)
    { 
        if (chan == -102938){
        //Examples of catching returned status codes from server
            if (msg == "PROCESSING"){
                //Our request made it through the queue and is now being processed
            
            } else if (msg == "INCOMPLETE"){
                //Player cannot hand in quest yet (Initiator Response)
                 llSay(0,"Did you find all of them?");
                
            } else if (msg == "ALREADY_DONE"){
                //Player already completed all objectives and handed in quest (Initiator Response)
                 llSay(0,"You already completed this quest");
                
            } else if (msg == "NOT_ENROLLED"){
                //Player has not enrolled yet (This is an objective response)
                
            } else if (msg == "WRONG_ORDER"){
                // Player has activated this objective before completing previous if this one is not 1st
                
            } else if (msg == "ENROLLED"){
                // Player has been enrolled on the quest (Initiator Response)
                 llSay(0,"Goo luck on your quest!");
                
            } else if (msg == "FINISHED"){
                // Quest Handed in successfully
                llSay(0,"You Did it!");
                
            } else if (msg == "SUCCESS"){
                // The request completed without error
                
            } else if (msg == "REWARD_ERROR"){
                // Server encountered a transaction error, player may try again later.
                     
            }
        }
    }
}
